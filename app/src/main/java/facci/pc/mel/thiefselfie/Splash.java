package facci.pc.mel.thiefselfie;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

public class Splash extends AppCompatActivity {

    TextView textView, textViewMensaje;
    ImageView imageView;

    Typeface fuenteA, fuenteB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        String fuente1 = "fuentes/fuente.otf";
        String fuente2 = "fuentes/fuente2.otf";

        this.fuenteA = Typeface.createFromAsset(getAssets(), fuente1);
        this.fuenteB = Typeface.createFromAsset(getAssets(), fuente2);

        textView = (TextView)findViewById(R.id.textView);
        textView.setTypeface(fuenteB);
        textViewMensaje = (TextView)findViewById(R.id.textViewMensaje);
        textViewMensaje.setTypeface(fuenteA);

        imageView = (ImageView)findViewById(R.id.imageView);

        Animation animation = AnimationUtils.loadAnimation(this, R.anim.transition);
        textView.startAnimation(animation);
        textViewMensaje.startAnimation(animation);
        imageView.startAnimation(animation);

        final Intent intent = new Intent(this, MainActivity.class);
        Thread timer = new Thread(){
            public void run (){
                try{
                   sleep(5000);
                }catch (InterruptedException e){
                    e.printStackTrace();
                }
                finally {
                    startActivity(intent);
                    finish();
                }
            }
        };
        timer.start();
    }
}
